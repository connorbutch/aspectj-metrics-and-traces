package com.connor.trace;

import com.amazonaws.xray.AWSXRay;
import com.amazonaws.xray.entities.Subsegment;

import java.util.Objects;

public class TraceProviderXrayImpl implements TraceProvider {
    private static final TraceProviderXrayImpl INSTANCE = new TraceProviderXrayImpl();

    public static TraceProvider getInstance() {
        return INSTANCE;
    }

    private TraceProviderXrayImpl() {
        //intentionally hidden/private
    }

    @Override
    public Trace createTrace(String traceName) {
        Objects.requireNonNull(traceName, "Trace name cannot be null");
        Subsegment subsegment = AWSXRay.beginSubsegment(traceName);
        return new TraceXrayImpl(subsegment);
    }
}
