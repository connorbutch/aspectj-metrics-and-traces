package com.connor.trace;

import com.amazonaws.xray.entities.Entity;

import java.util.Objects;

public class TraceXrayImpl implements Trace {
    private final Entity delegate;

    public TraceXrayImpl(Entity delegate) {
        Objects.requireNonNull(delegate, "Segment/subsegment cannot be null");
        this.delegate = delegate;
    }

    @Override
    public void close() throws Exception {
        delegate.close();
    }

    @Override
    public void putMetadata(final String key, final Object object) {
        delegate.putMetadata(key, object);
    }

    @Override
    public void putMetadata(final String namespace, final String key, final Object object) {
        delegate.putMetadata(namespace, key, object);
    }
}
