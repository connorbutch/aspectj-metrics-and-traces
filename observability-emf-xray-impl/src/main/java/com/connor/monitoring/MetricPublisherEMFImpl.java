package com.connor.monitoring;

import software.amazon.cloudwatchlogs.emf.logger.MetricsLogger;

import java.util.Map;

public class MetricPublisherEMFImpl implements MetricPublisher {
    //TODO store into map based on namespace as key
    @Override
    public void publishMetric(String namespace, String metricName, double metricValue, Map<String, String> dimensions) {
        MetricsLogger metricsLogger = new MetricsLogger();
        metricsLogger.setNamespace(namespace);
        metricsLogger.setDimensions(); //have to clear dimensions or it picks up log group name and stuff
        metricsLogger.putMetric(metricName, metricValue);
        metricsLogger.flush();
    }

    @Override
    public void publishMetric(String namespace, String metricName, long metricValue, Map<String, String> dimensions) {
        MetricsLogger metricsLogger = new MetricsLogger();
        metricsLogger.setNamespace(namespace);
        metricsLogger.setDimensions(); //have to clear dimensions or it picks up log group name and stuff
        metricsLogger.putMetric(metricName, metricValue);
        metricsLogger.flush();
    }
}
