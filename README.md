# order api
## Purpose


## Associated medium article
TODO associated medium article link

## Deploying the infrastructure
For your first time deploying the stack, use the following command:
```
sam build && sam deploy --guided
```

For subsequent deploys, run
```
sam build && sam deploy
```
## Generating traffic 
To generate traffic against the api (which will both succeed and receive a 2xx response as well as fail by getting a 5xx), run the following  command
```
./gradlew generateTraffic
```

## Viewing stats in cloudwatch dashboard
TODO