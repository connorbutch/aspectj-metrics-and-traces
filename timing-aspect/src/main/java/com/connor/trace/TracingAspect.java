package com.connor.trace;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;

import java.util.Optional;

@Aspect
public class TracingAspect {
    @Around("@annotation(com.connor.trace.Traced)")
    public Object callMethodAndStartTrace(ProceedingJoinPoint invocation)
            throws Throwable {
        MethodSignature methodSignature = (MethodSignature) invocation.getSignature();
        try (Trace trace = TraceProviderXrayImpl.getInstance().createTrace(getTraceName(methodSignature))) {
            Traced traced = methodSignature.getMethod().getAnnotation(Traced.class);
            if (traced.doLogParametersAsMetadata()) {
                logParametersAsMetadata(invocation, trace);
            }
            return invocation.proceed();
        }
    }

    private void logParametersAsMetadata(final ProceedingJoinPoint invocation, final Trace trace) {
        MethodSignature methodSignature = (MethodSignature) invocation.getSignature();
        for (int index = 0; index < methodSignature.getParameterNames().length; ++index) {
            trace.putMetadata(methodSignature.getParameterNames()[index], invocation.getArgs()[index]);
        }
    }

    private String getTraceName(MethodSignature methodSignature) {
        return Optional.of(methodSignature)
                .map(MethodSignature::getMethod)
                .map(method -> method.getAnnotation(Traced.class))
                .map(Traced::subsegmentName)
                .filter(subsegmentName -> !subsegmentName.isBlank())
                .orElseGet(methodSignature::getName);
    }
}
