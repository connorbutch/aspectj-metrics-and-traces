package com.connor.trace;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.CONSTRUCTOR})
public @interface Traced {
    /**
     * If the value is left as blank, the subsegment name will be the method name
     * @return
     */
    String subsegmentName() default "";

    boolean doLogParametersAsMetadata() default false;

    /**
     * If the value is left as blank, then the default metadata namespace is used
     * @return
     */
    String metadataNamespace() default "";
}
