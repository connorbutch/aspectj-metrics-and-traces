package com.connor.monitoring;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;

@Aspect
public class TimingAspect {
    private final MetricPublisher metricPublisher;

    public TimingAspect(MetricPublisher metricPublisher) {
        this.metricPublisher = metricPublisher;
    }

    public TimingAspect() {
        this(MetricPublisherFactory.getMetricPublisher());
    }

    @Around("@annotation(com.connor.monitoring.Timed)")
    public Object callMethodAndRecordLatencyMetrics(ProceedingJoinPoint invocation)
            throws Throwable {
        return callMethodAndRecordMetrics(invocation);
    }

    private Object callMethodAndRecordMetrics(ProceedingJoinPoint invocation) throws Throwable {
        MethodSignature methodSignature = (MethodSignature) invocation.getSignature();
        Timed timed = methodSignature.getMethod().getAnnotation(Timed.class);
        final long startTimeInMs = System.currentTimeMillis();
        Object obj = executeMethodAndHandleException(invocation, timed, startTimeInMs);
        final long endTimeInMs = System.currentTimeMillis();
        String latencyMetricName = getLatencyMetricName(timed);
        metricPublisher.publishMetric(timed.metricNamespace(), latencyMetricName, endTimeInMs - startTimeInMs);
        return obj;
    }

    private Object executeMethodAndHandleException(ProceedingJoinPoint invocation, Timed timed, long startTimeInMs) throws Throwable {
        try {
            return invocation.proceed();
        } catch (Exception e) {
            final long endTimeInMs = System.currentTimeMillis();
            handleException(timed, startTimeInMs, e, endTimeInMs);
            throw e;
        }
    }

    private void handleException(final Timed timed, final long startTimeInMs, final Exception e, final long endTimeInMs) {
        try {
            MetricExceptionHandler metricExceptionHandler = timed.exceptionHandler().getDeclaredConstructor().newInstance();
            metricExceptionHandler.handleException(e, timed, endTimeInMs - startTimeInMs);
        } catch (Exception ex) {
            System.out.println("Error creating exception metric handler");
            ex.printStackTrace();
        }
    }

    private String getLatencyMetricName(Timed timed) {
        String latencyMetricName = timed.latencyMetricName().trim();
        if (!latencyMetricName.endsWith("latency") && !latencyMetricName.endsWith("Latency")) {
            latencyMetricName = latencyMetricName + "Latency";
        }
        return latencyMetricName;
    }
}