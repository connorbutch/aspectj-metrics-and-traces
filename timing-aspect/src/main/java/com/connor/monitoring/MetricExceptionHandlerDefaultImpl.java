package com.connor.monitoring;

public class MetricExceptionHandlerDefaultImpl implements MetricExceptionHandler{
    private final MetricPublisher metricPublisher;

    public MetricExceptionHandlerDefaultImpl(MetricPublisher metricPublisher) {
        this.metricPublisher = metricPublisher;
    }

    public MetricExceptionHandlerDefaultImpl(){
        this(MetricPublisherFactory.getMetricPublisher());
    }

    @Override
    public void handleException(Exception thrown, Timed timedAnnotation, long methodExecutionTimeInMs) {
        System.out.println("Handling exception thrown in annotated method");
        String errorMetricName = getErrorLatencyMetricName(timedAnnotation);
        String namespace = timedAnnotation.metricNamespace();
        metricPublisher.publishMetric(namespace, errorMetricName, methodExecutionTimeInMs);
    }

    private String getErrorLatencyMetricName(Timed timedAnnotation) {
        String baseMetricName = timedAnnotation.latencyMetricName();
        if(baseMetricName.endsWith("latency") || baseMetricName.endsWith("Latency")){
            baseMetricName = baseMetricName.substring(0, baseMetricName.length() - "Latency".length());
        }
        return baseMetricName + "ErrorLatency";
    }
}
