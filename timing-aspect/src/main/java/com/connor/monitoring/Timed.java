package com.connor.monitoring;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Set;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.CONSTRUCTOR})
public @interface Timed {

    String metricNamespace();

    /**
     * There is no need to end this with "latency" -- if you do not include it, the framework will
     * automatically append "Latency" to the end of the value entered here
     * @return
     */
    String latencyMetricName();


    /**
     * The implementation class referenced here MUST contain a no args constructor for the framework
     * to be able to create an instance and use this to handle exceptions
     * @return
     */
    Class<? extends MetricExceptionHandler> exceptionHandler() default MetricExceptionHandlerDefaultImpl.class;
}
