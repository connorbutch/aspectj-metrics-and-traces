package com.connor.monitoring;

public interface MetricExceptionHandler {
    void handleException(Exception thrown, Timed timedAnnotation, long methodExecutionTimeInMs);
}
