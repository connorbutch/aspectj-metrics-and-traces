package com.connor.trace;

public interface Trace extends AutoCloseable {
    void putMetadata(String key, Object object);
    void putMetadata(String namespace, String key, Object object);
}
