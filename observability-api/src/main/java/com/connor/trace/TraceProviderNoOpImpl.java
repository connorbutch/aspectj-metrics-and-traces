package com.connor.trace;

public class TraceProviderNoOpImpl implements TraceProvider{
    @Override
    public Trace createTrace(String traceName) {
        return new Trace() {
            @Override
            public void putMetadata(final String key, final Object object) {

            }

            @Override
            public void putMetadata(final String namespace, final String key, final Object object) {

            }

            @Override
            public void close() throws Exception {

            }
        };
    }
}
