package com.connor.trace;

public interface TraceProvider {
    Trace createTrace(String traceName);
}
