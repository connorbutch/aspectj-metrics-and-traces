package com.connor.monitoring;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class MetricPublisherFactory {
    private static final String METRIC_PUBLISHER_ENV_KEY = "METRIC_PUBLISHER_CLASSPATH";
    private static final String DEFAULT_METRIC_PUBLISHER_CLASSPATH = "com.connor.monitoring.MetricPublisherEMFImpl" /*"com.connor.monitoring.MetricPublisherNoOpImpl"*/; //debate whether to use no op impl or emf by default

    //reflection is extremely slow, so at least store in a cache as a short-term solution
    //longer-term, would suggest setting this in a factory itself or something
    private final static Map<String, MetricPublisher> fullyQualifiedNameToInstanceCache = new HashMap<>();

    public static MetricPublisher getMetricPublisher() {
        String fullyQualifiedClassPath = getFullyQualifiedClasspath();
        return fullyQualifiedNameToInstanceCache.computeIfAbsent(fullyQualifiedClassPath, fullyQualifiedClassPathVar -> getMetricPublisherWithReflection(fullyQualifiedClassPath));
    }

    private static MetricPublisher getMetricPublisherWithReflection(String fullyQualifiedClassPath) {
        try {
            return (MetricPublisher) Class.forName(fullyQualifiedClassPath).getDeclaredConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException |
                 ClassNotFoundException e) {
            final String errorMessage = String.format("Error instantiating class using default no arg constructor: %s", fullyQualifiedClassPath);
            throw new RuntimeException(errorMessage, e);
        }
    }

    private static String getFullyQualifiedClasspath() {
        return getClassPathFromEnvVariable().orElse(DEFAULT_METRIC_PUBLISHER_CLASSPATH);
    }

    private static Optional<String> getClassPathFromEnvVariable() {
        return Optional.ofNullable(System.getenv(METRIC_PUBLISHER_ENV_KEY))
                .filter(value -> !value.isBlank());
    }
}
