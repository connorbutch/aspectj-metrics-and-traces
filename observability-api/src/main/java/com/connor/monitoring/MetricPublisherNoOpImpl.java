package com.connor.monitoring;

import java.util.Map;

class MetricPublisherNoOpImpl implements MetricPublisher {
    @Override
    public void publishMetric(String namespace, String metricName, double metricValue, Map<String, String> dimensions) {
        System.out.printf("NoOp impl for publishing double for key %s and value %f", metricName, metricValue);
    }

    @Override
    public void publishMetric(String namespace, String metricName, long metricValue, Map<String, String> dimensions) {
        System.out.printf("NoOp impl for publishing long for key %s and value %d", metricName, metricValue);
    }
}
