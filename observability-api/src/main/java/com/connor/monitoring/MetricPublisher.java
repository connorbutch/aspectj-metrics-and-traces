package com.connor.monitoring;

import java.util.Collections;
import java.util.Map;

public interface MetricPublisher {
    default void publishMetric(String namespace, String metricName, double metricValue){
        publishMetric(namespace, metricName, metricValue, Collections.emptyMap());
    }

    void publishMetric(String namespace, String metricName, double metricValue, Map<String, String> dimensions);

    default void publishMetric(String namespace, String metricName, long metricValue){
        publishMetric(namespace, metricName, metricValue, Collections.emptyMap());
    }

    void publishMetric(String namespace, String metricName, long metricValue, Map<String, String> dimensions);
}
