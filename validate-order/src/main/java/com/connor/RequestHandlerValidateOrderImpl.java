package com.connor;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.SendMessageResult;
import com.connor.monitoring.Timed;
import com.connor.trace.Traced;

public class RequestHandlerValidateOrderImpl implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {
    private static final int MINIMUM_SIMULATED_LATENCY = 200;
    private static final int MAXIMUM_SIMULATED_LATENCY = 5000;
    private static final String QUEUE_URL = System.getenv("QUEUE_URL");

    private final AmazonSQS amazonSQS;

    public RequestHandlerValidateOrderImpl(AmazonSQS amazonSQS) {
        this.amazonSQS = amazonSQS;
    }

    public RequestHandlerValidateOrderImpl() {
        this(AmazonSQSClientBuilder.defaultClient());
    }

    public APIGatewayProxyResponseEvent handleRequest(final APIGatewayProxyRequestEvent input, final Context context) {
        validateAddress(input);
        SendMessageResult sendMessageResult = amazonSQS.sendMessage(QUEUE_URL, input.getBody()); //NOTE: you would really make this idempotent....
        return new APIGatewayProxyResponseEvent()
                .withStatusCode(200)
                .withBody(input.getBody()); //NOTE: you would really return an id here as well for the order
    }

    @Timed(metricNamespace = "address", latencyMetricName = "validateAddressApi")
    @Traced(doLogParametersAsMetadata = true)
    private void validateAddress(APIGatewayProxyRequestEvent input) {
        simulateLatency();
        if (shouldThrowException(input)) {
            throw new RuntimeException("Oh no, something went wrong");
        }
    }

    private boolean shouldThrowException(APIGatewayProxyRequestEvent input) {
        return input != null && input.getHeaders() != null && input.getHeaders().containsKey("x-throw-exception");
    }

    @Traced(subsegmentName = "transformResponse")
    private void simulateLatency() {
        try {
            Thread.sleep(getRandomNumberInRange(MINIMUM_SIMULATED_LATENCY, MAXIMUM_SIMULATED_LATENCY));
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException("Thread was interrupted when sleeping to simulate latency", e);
        }
    }

    private int getRandomNumberInRange(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }
}
