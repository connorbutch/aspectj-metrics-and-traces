#!/bin/sh
file="../samconfig.toml"

if [ -f "$file" ]
then
  while IFS='=' read -r key value
  do
    key=$(echo $key | tr '.' '_')

    eval ${key}=\${value} 2>/dev/null || :
  done < "$file"
  regionWithoutQuotes=$(eval echo $region)
  stackNameWithoutQuotes=$(eval echo $stack_name)
  orderUrl=`aws cloudformation describe-stacks --stack-name ${stackNameWithoutQuotes} --region ${regionWithoutQuotes} --query "Stacks[0].Outputs[?OutputKey=='OrderApiUrl'].OutputValue" --output text`
else
  echo "$file not found."
fi
newman run ../postman/OrderTimePostmanCollection.json --global-var "orderUrl=$orderUrl"